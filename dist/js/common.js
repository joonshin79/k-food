$(function() {

    var $section = $('[data-section]');
    var $module = $section.data('section');
    var kFood = {}
    kFood.modules = {
        gnb: function() {
            var $header = $('header');
            var $gnb = $header.find('.gnb');
            $gnb.find('>nav>button').mouseenter(function(e) {
                e.preventDefault();
                var item1 = $(this).data('gnb-btn');
                $gnb.find('[data-gnb-list]').each(function(index, item) {
                    var item2 = $(this).data('gnb-list');
                    if (item1 === item2) {
                        $(this).css('display', 'flex');
                        $(this).mouseleave(function(e) {
                            e.preventDefault();
                            $(this).hide();

                        });
                    } else {
                        $(this).hide();
                    }
                    // GNB 브랜몰 초기화
                    if (item2 === 'brand') {
                        $gnb.find('[data-gnb-brand]').show();
                        $gnb.find('[data-gnb-category]').each(function(index) {
                            if (index === 0) {
                                $(this).addClass('active');
                            } else {
                                $(this).removeClass('active');
                            }
                        });
                        $gnb.find('[data-gnb-string]').each(function(index) {
                            var string = $(this).data('gnb-string');
                            if (string === 'ㄱ,kor_intl') {
                                $(this).addClass('active');
                            } else {
                                $(this).removeClass('active');
                            }
                        });
                        $gnb.find('[data-gnb-tab]').each(function(index) {
                            if (index === 0) {
                                $(this).addClass('active');
                            } else {
                                $(this).removeClass('active');
                            }
                        });
                        $gnb.find('[data-gnb-article]').each(function(index) {
                            if (index === 0) {
                                $(this).css('display', 'flex');
                            } else {
                                $(this).hide();
                            }
                        });
                    }
                });
            });
            // GNB 브랜몰 탭
            $gnb.find('[data-gnb-tab]').click(function(e) {
                e.preventDefault();
                var tab = $(this).data('gnb-tab');
                $gnb.find('[data-gnb-tab]').removeClass('active');
                $(this).addClass('active');
                $gnb.find('[data-gnb-article]').each(function(index, item) {
                    var article = $(this).data('gnb-article');
                    if (tab === article) {
                        if (article === 'string') {
                            $(this).show();
                            $gnb.find('[data-gnb-brand]').css('display', 'flex');
                        } else if (article === 'store') {
                            $(this).css('display', 'flex');
                            $gnb.find('[data-gnb-brand]').hide();
                        } else {
                            $(this).css('display', 'flex');
                            $gnb.find('[data-gnb-brand]').css('display', 'flex');
                        }
                    } else {
                        $(this).hide();
                    }
                });
            });
            // GNB 브랜몰 String Sort
            $gnb.find('[data-gnb-string]').click(function(e) {
                e.preventDefault();
                var string = $(this).data('gnb-string');
                $gnb.find('[data-gnb-string]').removeClass('active');
                $(this).addClass('active');

                console.log(string);
            });

            // GNB 브랜몰 Category Sort
            $gnb.find('[data-gnb-category]').click(function(e) {
                e.preventDefault();
                var category = $(this).data('gnb-category');
                $gnb.find('[data-gnb-category]').removeClass('active');
                $(this).addClass('active');

                console.log(category);
            });
        },
        searchBox: function() {
            var $header = $('header');
            // Search Start
            var $searchBox = $header.find('.searchBox');
            $searchBox.find('>.select>a').click(function(e) {
                e.preventDefault();
                if ($(this).next('ul').is(':visible') == true) {
                    $(this).next('ul').slideUp(300);
                } else {
                    $(this).next('ul').slideDown(300);
                }
                $searchBox.find('>.select>ul a').click(function(e) {
                    e.preventDefault();
                    var text = $(this).text();
                    var value = $(this).data('value');
                    var _this = $(this).parents('ul').prev('a');
                    $(this).parents('ul').slideUp();
                    _this.removeData('quantity-price').attr('data-value', value);
                    _this.text(text);
                    _this.attr('data-value', value);
                });
            });
            // 검색버튼 클릭
            $searchBox.find('>button').click(function(e) {
                e.preventDefault();
                var value = $searchBox.find('[data-value]').data('value');
                var text = $searchBox.find('input[type=text]').val();

                console.log(value); // 셀렉트
                console.log(text); // 검색어
            });
        },
        listSearch: function() {
            // 조회기간 Start
            $listSearch = $this.find('.list-search');
            $listSearch.find('.btns>button').click(function(e) {
                e.preventDefault();
                var value = $(this).data('list-date');

                $listSearch.find('.btns>button').removeClass('active');
                $(this).addClass('active');

                console.log(value); // 날짜
            });

            $listSearch.find('.calendar>button').click(function(e) {
                e.preventDefault();
                var value1 = $listSearch.find('input[type=text').eq(0).val()
                var value2 = $listSearch.find('input[type=text').eq(1).val()

                console.log(value1); // 날짜1
                console.log(value2); // 날짜2
            });
            // 조회기간 End

        },
        datepicker: function() {
            // datepicker Start
            var $datepicker = $this.find('.datepicker');
            $datepicker.each(function(index) {
                $(this).datepicker();
                $.datepicker.setDefaults({
                    closeText: "닫기",
                    prevText: "이전달",
                    nextText: "다음달",
                    currentText: "오늘",
                    monthNames: ["1월(JAN)", "2월(FEB)", "3월(MAR)", "4월(APR)", "5월(MAY)", "6월(JUN)", "7월(JUL)", "8월(AUG)", "9월(SEP)", "10월(OCT)", "11월(NOV)", "12월(DEC)"],
                    monthNamesShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
                    dayNames: ["일", "월", "화", "수", "목", "금", "토"],
                    dayNamesShort: ["일", "월", "화", "수", "목", "금", "토"],
                    dayNamesMin: ["일", "월", "화", "수", "목", "금", "토"],
                    changeMonth: true, // month 셀렉트박스 사용
                    changeYear: true, // year 셀렉트박스 사용
                    weekHeader: "Wk",
                    dateFormat: "yymmdd",
                    firstDay: 0,
                    isRTL: false,
                    showMonthAfterYear: true,
                    yearSuffix: ""
                });
            });
            // datepicker End
        },
        fileTarget: function() {
            //파일선택
            var fileTarget = $this.find('.inputFile>input[type=file]');
            fileTarget.on('change', function() {
                if (window.FileReader) {
                    var filename = $(this)[0].files[0].name;
                } else {
                    var filename = $(this).val().split('/').pop().split('\\').pop();
                }
                $(this).siblings('input[type=text]').val(filename);
            });
        }
    }
    kFood.modules.gnb();
    kFood.modules.searchBox();

    var $this = $section;
    // Main Start
    if ($module == 'main') {
        var swiper = new Swiper('.keyVisual', {
            simulateTouch: false,
            effect: 'fade',
            slidesPerView: 1,
            slidesPerGroup: 1,
            loop: true,
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.keyVisual .pagination',
                clickable: true,
            },
        });

        var swiper = new Swiper('.brand-swiper', {
            simulateTouch: false,
            slidesPerView: 1,
            slidesPerGroup: 1,
            loop: true,
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.brand-swiper .pagination',
                clickable: true,
            },
        });

        var swiper = new Swiper('.sticky', {
            slidesPerView: 'auto',
            spaceBetween: 30,
            navigation: {
                prevEl: '.sticky .prev',
                nextEl: '.sticky .next',
            },
        });

        $this.find('[data-bestitem-string]').click(function(e) {
            e.preventDefault();
            var string = $(this).data('bestitem-string');
            $this.find('[data-bestitem-string]').removeClass('active');
            $(this).addClass('active');

            console.log(string);
        });

        var brandMall = $this.find('[data-brand-mall]');
        brandMall.find('[data-banner-src]').click(function(e) {
            e.preventDefault();
            var src = $(this).data('banner-src');
            brandMall.find('[data-banner-src]').removeClass('active');
            $(this).addClass('active');
            brandMall.find('.banner>img').attr('src', src);
        });

        var swiper = new Swiper('[data-brand-mall] .swiper-container', {
            simulateTouch: false,
            slidesPerView: 2,
            slidesPerGroup: 2,
            loop: true,
            navigation: {
                nextEl: '[data-brand-mall] .next',
                prevEl: '[data-brand-mall] .prev',
            },
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            }
        });

    }
    // Main End

    // Login Start
    if ($module == 'login') {}
    // Login End

    // Mypage Start
    if ($module == 'mypage') {
        kFood.modules.listSearch();
        kFood.modules.datepicker();
        kFood.modules.fileTarget();

        // 메뉴 삭제
        var $btnMenuDel = (function() {
            $this.find('[data-menu-del]').click(function(e) {
                e.preventDefault();
                $(this).parents('tr').remove();
                var _list = $this.find('[data-menu-list]>tbody>tr');
                _list.each(function(index) {
                    index += 1;
                    $(this).find('>td:first-child').text(index);
                });
            });
        });
        $btnMenuDel();

        // 메뉴 추가
        var $btnMenuAdd = $this.find('[data-menu-add]');
        $btnMenuAdd.click(function(e) {
            e.preventDefault();
            var _this = $this.find('[data-menu-list]>tbody');
            var i = _this.find('>tr').length;
            i += 1;
            var html = '<tr>';
            html += '<td>' + i + '</td>';
            html += '<td class="subject"><input type="text" style="width: 100%;"></td>';
            html += '<td><button class="btn_del" data-menu-del="">삭제</button></td>';
            html += '</tr>';
            _this.append(html);
            $btnMenuDel();
        });

        // 옵션 추가
        var $btnMenuAdd = $this.find('[data-option-add]');
        $btnMenuAdd.click(function(e) {
            e.preventDefault();
            var _this = $this.find('.option-add').parents('td');
            var html = '<div class="option-add">';
            html += '<input type="text" placeholder="상품명 (단위/수량)">';
            html += '<input type="text" placeholder="금액">';
            html += '</div>';
            _this.append(html);
        });
    }
    // Mypage End

    // Product Start
    if ($module == 'product') {
        // 썸네일 Swiper
        var swiper = new Swiper('.optImage .swiper-container', {
            simulateTouch: false,
            slidesPerView: 4,
            spaceBetween: 10,
            navigation: {
                nextEl: '.optImage .next',
                prevEl: '.optImage .prev',
            }
        });

        // 썸네일 클릭시 메인이미지 변경
        var optImage = $this.find('.optImage');
        optImage.find('.swiper-slide').click(function(e) {
            e.preventDefault();
            var src = $(this).find('>img').attr('src');
            optImage.find('.large>img').attr("src", src);
        });


        // 추천상품 / 인기상품
        $this.find('.recommend-wrap .item').each(function(index) {
            var _this = $(this);
            var next = _this.find('.next');
            var prev = _this.find('.prev');
            var swiper = new Swiper(_this, {
                simulateTouch: false,
                slidesPerView: 3,
                slidesPerGroup: 3,
                spaceBetween: 10,
                navigation: {
                    nextEl: next,
                    prevEl: prev,
                }
            });
        });
        // 탭 클리시 포커스 이동
        $this.find('[data-btn-contents]').click(function(e) {
            e.preventDefault();
            var btn = $(this).data('btn-contents');
            $this.find('[data-contents]').each(function(index, item) {
                var data = $(this).data('contents');
                if (btn === data) {
                    var offset = $(this).offset();
                    $('html, body').animate({ scrollTop: offset.top }, 400);
                }
            });
        });

        // QNA
        var qnaLink = $this.find('.qna .tbody .col_title>a')
        qnaLink.click(function(e) {
            e.preventDefault();
            var reply = $(this).parents('.tbody').next('.reply');
            if (reply.parents('li').hasClass('secret') === true) {
                alert('비공개 문의내역은 작성자 본인만 확인하실 수 있습니다.');
            } else if (reply.parents('li').hasClass('secret') === false && reply.is(':visible') == true) {
                reply.hide();
            } else {
                reply.css('display', 'flex');
            }
        });


    }
    // Product End

    // rivew Start
    var $popup = $('[data-popup]').data('popup');
    if ($popup == 'rivew') {
        var star = $('.star>label');
        star.click(function(e) {
            e.preventDefault();
            var item = $(this).attr('for');
            if ($(this).hasClass('active') === true) {
                star.removeClass('active');
            } else {
                star.each(function(index) {
                    if (index < item) {
                        $(this).addClass('active');
                    }
                });
            }
        });

    }
    // rivew End

});