$(function() {
    var $section = $('[data-section]');
    var $module = $section.data('section');

    var kFood = {}
    kFood.modules = {
        navi: function() {
            var navi = $('nav');
            $('[data-navi]').click(function(e) {
                e.preventDefault();
                var data = $(this).data('navi');
                if (data === 'open') {
                    navi.show();
                    $('body').css('overflow', 'hidden');
                } else {
                    navi.hide();
                    $('body').css('overflow', 'unset');
                }
            });


            navi.find('[data-gnb-btn]').click(function(e) {
                e.preventDefault();
                navi.find('[data-gnb-btn]').removeClass('active');
                $(this).addClass('active');

                var btn = $(this).data('gnb-btn');
                navi.find('[data-gnb-list]').each(function(index, item) {
                    var list = $(this).data('gnb-list');
                    if (btn === list) {
                        $(this).addClass('show');
                    } else {
                        $(this).removeClass('show');
                    }
                });
            });

            navi.find('[data-gnb-tab]').click(function(e) {
                e.preventDefault();
                navi.find('[data-gnb-tab]').removeClass('active');
                $(this).addClass('active');

                var tab = $(this).data('gnb-tab');
                navi.find('[data-gnb-article]').each(function(index, item) {
                    var article = $(this).data('gnb-article');
                    if (tab === article) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            });

            var categoryH3 = navi.find('[data-gnb-list^="category"] h3');
            categoryH3.click(function(e) {
                e.preventDefault();
                if ($(this).next('ul').is(':visible') == true) {
                    $(this).next('ul').slideUp(300);
                    $(this).removeClass('active');
                } else {
                    categoryH3.removeClass('active');
                    categoryH3.next('ul').slideUp(300);
                    $(this).next('ul').slideDown(300);
                    $(this).addClass('active');
                }
            });
        },
        fileTarget: function() {
            //파일선택
            var fileTarget = $this.find('.inputFile>input[type=file]');
            fileTarget.on('change', function() {
                if (window.FileReader) {
                    var filename = $(this)[0].files[0].name;
                } else {
                    var filename = $(this).val().split('/').pop().split('\\').pop();
                }
                $(this).siblings('input[type=text]').val(filename);
            });
        }
    }
    kFood.modules.navi();

    var $this = $section;

    // Main Start
    if ($module == 'main') {
        $(window).scroll(function() {
            var scrollPos = $(window).scrollTop();
            if (scrollPos > 100) {
                $('.sticky').addClass('fixed');
            } else {
                $('.sticky').removeClass('fixed');
            }
        });

        var brandMall = $this.find('[data-brand-mall]');
        brandMall.find('[data-banner-src]').click(function(e) {
            e.preventDefault();
            var src = $(this).data('banner-src');
            brandMall.find('[data-banner-src]').removeClass('active');
            $(this).addClass('active');
            brandMall.find('.banner>img').attr('src', src);
        });

        var swiper = new Swiper('.keyVisual', {
            loop: true,
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.keyVisual .pagination',
                clickable: true,
            },
        });
        var swiper = new Swiper('.brand-swiper', {
            loop: true,
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.brand-swiper .pagination',
                clickable: true,
            },
        });

        var swiper = new Swiper('[data-brand-mall] .swiper-container', {
            slidesPerView: 2,
            slidesPerGroup: 2,
            loop: true,
            pagination: {
                el: '[data-brand-mall] .swiper-container .pagination',
                clickable: true,
            },
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            }
        });
        var swiper = new Swiper('header .sticky', {
            slidesPerView: 'auto',
            spaceBetween: 20,
            navigation: {
                nextEl: 'header .sticky .next',
            },
        });
    }
    // Main End

    // product Start
    if ($module == 'product') {
        var swiper = new Swiper('.optImage', {
            loop: true,
            pagination: {
                el: '.optImage .pagination',
                clickable: true,
            },
        });

        // 추천상품 / 인기상품
        $this.find('.recommend-wrap .item').each(function(index) {
            var _this = $(this);
            var pagination = _this.find('.pagination');
            var swiper = new Swiper(_this, {
                slidesPerView: 3,
                slidesPerGroup: 3,
                spaceBetween: 10,
                pagination: {
                    el: pagination,
                    clickable: true,
                },
            });
        });

        // 탭 클리시 포커스 이동
        $this.find('[data-btn-contents]').click(function(e) {
            e.preventDefault();
            var btn = $(this).data('btn-contents');
            $this.find('[data-btn-contents]').removeClass('active');
            $(this).addClass('active');
            $this.find('[data-contents]').each(function(index, item) {
                var data = $(this).data('contents');
                if (btn === data) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });

        // 스크롤 탭 고정
        $(window).scroll(function() {
            var scrollPos = $(window).scrollTop();
            var t1 = $this.find('.optImage').outerHeight();
            var t2 = $this.find('.optInfo').outerHeight();
            var t3 = t1 + t2 + 50;
            if (scrollPos > t3) {
                $this.find('.tab').addClass('fixed');
            } else {
                $this.find('.tab').removeClass('fixed');
            }
        });

        // 장바구니, 바로구매 버튼 클릭시
        $this.find('[data-action-type]').click(function(e) {
            e.preventDefault();
            var data = $(this).data('action-type');
            if (data == 'cart' || data == 'buy') {
                $this.find('.optionContainer').animate({
                    bottom: "0"
                }, 300);
                $('body').css('overflow', 'hidden');
            } else if (data == 'close') {
                $this.find('.optionContainer').animate({
                    bottom: "-100vh"
                }, 300);
                $('body').css('overflow', 'unset');
            }
        });

        // QNA
        var qnaLink = $this.find('.qna .tbody .col_title>a')
        qnaLink.click(function(e) {
            e.preventDefault();
            var reply = $(this).parents('.tbody').next('.reply');
            if (reply.parents('li').hasClass('secret') === true) {
                alert('비공개 문의내역은 작성자 본인만 확인하실 수 있습니다.');
            } else if (reply.parents('li').hasClass('secret') === false && reply.is(':visible') == true) {
                reply.hide();
            } else {
                reply.css('display', 'flex');
            }
        });

    }
    // product End


    // Mypage Start
    if ($module == 'mypage') {
        kFood.modules.fileTarget();

        // 메뉴 삭제
        var $btnMenuDel = (function() {
            $this.find('[data-menu-del]').click(function(e) {
                e.preventDefault();
                $(this).parents('dl').remove();
                var _list = $this.find('[data-menu-list]>dl');
                _list.each(function(index) {
                    index += 1;
                    $(this).find('>dd>.num').text(index);
                });
            });
        });
        $btnMenuDel();

        // 메뉴 추가
        var $btnMenuAdd = $this.find('[data-menu-add]');
        $btnMenuAdd.click(function(e) {
            e.preventDefault();
            var _this = $this.find('[data-menu-list]');
            var i = _this.find('>dl').length;
            i += 1;
            console.log();
            var html = '<dl>';
            html += '<dt>이름</dt>';
            html += '<dd><span class="num">' + i + '.</span><input type="text" style="width: calc(100% - 70px); margin-right: 5px;"><button class="btn_del" data-menu-del="">삭제</button></td>';
            html += '</dl>';
            _this.append(html);
            $btnMenuDel();
        });

        // 옵션 추가
        var $btnMenuAdd = $this.find('[data-option-add]');
        $btnMenuAdd.click(function(e) {
            e.preventDefault();
            var _this = $this.find('.option-add').parents('td');
            var html = '<div class="option-add">';
            html += '<input type="text" placeholder="상품명 (단위/수량)">';
            html += '<input type="text" placeholder="금액">';
            html += '</div>';
            _this.append(html);
        });
    }
    // Mypage End

    if ($module == 'register' || $module == 'sign-up' || $module == 'order' || $module == 'info') {
        $('footer address').css('padding-bottom', '15px');
    }
});